package com.yoki.advent_of_code.aoc.days2021;

import static java.lang.Math.abs;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.algo.Matrix;
import com.yoki.advent_of_code.utils.vector.Vector;
import com.yoki.advent_of_code.utils.CollectionUtil;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.tuple.Pair;

public class Day19 extends AocDay {

  public static final Matrix TX = new Matrix(
      1,0,0,
      0,0,1,
      0,-1,0);

  public static final Matrix TY = new Matrix(
      0, 0, -1,
      0, 1, 0,
      1, 0, 0);

  public static final Matrix TZ = new Matrix(
      0, 1, 0,
      -1, 0, 0,
      0, 0, 1);

  private final List<String> ip;
  private Map<Integer, Vector> allPoss;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day19(String input, PrintStream output) {
    super(input, output);
    this.ip = this.input.lines().toList();
  }

  public String part1() {
    List<Object> res = fullBeaconList(loadScanners(ip));
    Set<Vector> vectors = (Set<Vector>) res.get(0);
    this.allPoss = (Map<Integer, Vector>) res.get(1);
    return String.valueOf(vectors.size());
  }

  public String part2() {
    double max = 0;
    for (var pos : allPoss.values()) {
      for (var pos2 : allPoss.values()) {
        if (!pos.equals(pos2)) {
          max = Math.max(max, manhattanDistance(pos, pos2));
        }
      }
    }
    return String.valueOf(max);
  }

  public double manhattanDistance(Vector v1, Vector v2) {
    return abs(v1.get(0) - v2.get(0)) + abs(v1.get(1) - v2.get(1)) + abs(v1.get(2) - v2.get(2));
  }

  public List<Object> fullBeaconList(List<Scanner> scanners) {

    Deque<Scanner> toStudy = CollectionUtil.asStack(scanners.get(0));
    scanners.remove(0);
    Set<Scanner> nonReached = new HashSet<>(scanners);
    Set<Scanner> studied = new HashSet<>();

    Map<Integer, Vector> allPosses = new HashMap<>();
    while (!nonReached.isEmpty()) {
      Scanner source = toStudy.pop();
      for (var scanner : nonReached) {
        Pair<Vector, Matrix> founds = getCommonsBeacons(source, scanner);
        if (founds != null) {
          Vector tr = founds.getLeft();
          Matrix rot = founds.getRight();
          toStudy.add(
              new Scanner(StreamEx.of(scanner.beacons).map(b -> new Vector(rot.mul(b).add(tr).zip())).toSet(), scanner.index));
          allPosses.put(scanner.index, tr);
        }
      }
      studied.add(source);
      nonReached = StreamEx.of(nonReached).filter(s -> toStudy.stream().map(ss -> ss.index).noneMatch(n -> n == s.index)).toSet();
    }

    Set<Vector> allBeacons = new HashSet<>();
    for (Scanner data : toStudy) {
      allBeacons.addAll(data.beacons);
    }
    for (Scanner steps : studied) {
      allBeacons.addAll(steps.beacons);
    }
    return List.of(allBeacons, allPosses);
  }

  public Pair<Vector, Matrix> getCommonsBeacons(Scanner origin, Scanner other) {
    for (Matrix m : getAllTransfos()) {
      // ref-transformed = (ref, original)
      Map<Vector, List<Pair<Vector, Vector>>> result = new HashMap<>();
      for (var ref : origin.beacons) {
        for (var t : StreamEx.of(other.beacons).map(v -> Pair.of(v, m.mul(v))).toList()) {
          Vector key = ref.sub(t.getValue());
          result.putIfAbsent(key, new ArrayList<>());
          result.get(key).add(Pair.of(ref, t.getKey()));
        }
      }
      for (var e : result.entrySet()) {
        if (e.getValue().size() > 11) {
          Pair<Vector, Matrix> verify = verify(origin.beacons, other.beacons, e.getKey(), m);
          if (verify != null) return verify;
        }
      }
    }
    return null;
  }

  public Pair<Vector, Matrix> verify(Set<Vector> source, Set<Vector> other, Vector translation, Matrix rotation) {
    Set<Vector> sourceSet = new HashSet<>(source);
    Set<Vector> translatedBeacons = StreamEx.of(other).map(b -> rotation.mul(b).add(translation)).toSet();
    int i = 0;
    for (var v : translatedBeacons) {
      if (sourceSet.contains(v)) i++;
    }
    if (i < 12) return null;
    return Pair.of(translation, rotation);
  }

  public Set<Matrix> getAllTransfos() {
    Set<Matrix> allTransfos = new HashSet<>();
    for (int alpha : List.of(0, 1, 2, 3)) {
      for (int beta : List.of(0, 1, 2, 3)) {
        for (int gamma : List.of(0, 1, 2, 3)) {
          allTransfos.add(TX.pow(alpha).mul(TY.pow(beta)).mul(TZ.pow(gamma)));
        }
      }
    }
    return allTransfos;
  }

  public Matrix invert(Matrix mat) {
    if (mat == mat.id()) return mat;
    for (int a : List.of(0, 1, 2, 3)) {
      for (int b : List.of(0, 1, 2, 3)) {
        for (int c : List.of(0, 1, 2, 3)) {
          var potentialInvert = TX.pow(a).mul(TY.pow(b)).mul(TZ.pow(c));
          if (potentialInvert.mul(mat) == mat.id())
            return potentialInvert;
        }
      }
    }

    throw new Error("Could not invert matrix " + mat + ": maybe it is not an element of the group?");
  }

  private List<Scanner> loadScanners(List<String> ip) {
    List<Scanner> scanners = new ArrayList<>();
    List<String> curr = new ArrayList<>();
    int i = 0;
    for (String line : ip) {
      if (!curr.isEmpty() && line.startsWith("---")) {
        curr.remove(curr.size() - 1);
        scanners.add(new Scanner(curr, i++));
        curr = new ArrayList<>();
      }
      curr.add(line);
    }
    scanners.add(new Scanner(curr, i));
    return scanners;
  }

  static class Scanner {

    public final int index;
    public final Set<Vector> beacons;

    public Scanner(List<String> beacons, int index) {
      this.index = index;
      this.beacons = StreamEx.of(beacons).skip(1).map(s ->
          new Vector(Arrays.stream(s.split(",")).map(Double::parseDouble).toList())
      ).toSet();
    }

    public Scanner(Set<Vector> beacons, int index) {
      this.beacons = beacons;
      this.index = index;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Scanner scanner = (Scanner) o;
      return index == scanner.index && Objects.equals(beacons, scanner.beacons);
    }

    @Override
    public int hashCode() {
      return Objects.hash(index, beacons);
    }
  }
}
