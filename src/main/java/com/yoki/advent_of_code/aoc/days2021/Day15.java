package com.yoki.advent_of_code.aoc.days2021;

import static java.util.stream.Collectors.toList;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Set;

public class Day15 extends AocDay {

  private final int[][] map;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day15(String input, PrintStream output) {
    super(input, output);
    var outputValues = this.input.lines()
        .map(l -> l.chars().mapToObj(c -> (char)c).map(Object::toString).map(Integer::parseInt)
            .collect(toList())).collect(toList());

    int size = outputValues.size();
    this.map = new int[size][size];
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        map[i][j] = outputValues.get(i).get(j);
      }
    }
  }

  public String part1() {
    return String.valueOf(findLowestRisk(map));
  }

  public String part2() {
    int[][] extendedMap = extendBothSide(map, 5);
    return String.valueOf(findLowestRisk(extendedMap));
  }

  private int[][] extendBothSide(int[][] map, int n) {
    int size = map.length;
    int[][] extendedMap = new int[size * n][size * n];
    for (int l = 0; l < n; l++) {
      for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
          for (int k = 0; k < n; k++) {
            int x = modNonZero(map[i][j] + k + l, 9);
            extendedMap[i + k * size][j + l * size] = x;
            extendedMap[i + l * size][j + k * size] = x;
          }
        }
      }
    }
    return extendedMap;
  }

  private int modNonZero(int n, int mod) {
    return (n - 1) % mod + 1;
  }

  private int findLowestRisk(int[][] map) {
    Graph graph = new Graph(map);
    graph.dijkstra();
    return graph.getLast().distance;
  }

  static class Graph {

    Node[] nodes;

    public Graph(int[][] map) {
      int size = map.length;
      nodes = new Node[size * size];
      for (int i = 0; i < size; i++) {
        for (int j = 0; j < map[0].length; j++) {
          int s = j + i * size;
          if (i + 1 < size)
            add(s, j + (i + 1) * size, map[i + 1][j]);
          if (j + 1 < map[0].length)
            add(s, j + 1 + i * size, map[i][j + 1]);
          if (i - 1 > 0)
            add(s, j + (i - 1) * size, map[i - 1][j]);
          if (j - 1 > 0)
            add(s, j - 1 + i * size, map[i][j - 1]);
        }
      }
    }

    private void add(int a, int b, int d) {
      Node nodeA = addNode(a);
      Node nodeB = addNode(b);
      nodeA.addDestination(nodeB, d);
    }

    private Node addNode(int n) {
      if (nodes[n] == null) {
        nodes[n] = new Node(n);
      }
      return nodes[n];
    }

    public Node getLast() {
      return nodes[nodes.length - 1];
    }

    public void dijkstra() {
      dijkstra(0);
    }

    public void dijkstra(int n) {
      Node source = nodes[n];
      source.distance = 0;

      Set<Integer> settledNodes = new HashSet<>();
      PriorityQueue<Node> pq = new PriorityQueue<>(nodes.length, new Node());
      pq.add(source);

      while (!pq.isEmpty()) {
        Node currentNode = pq.remove();
        int number = currentNode.number;
        if (settledNodes.contains(number)) continue;

        settledNodes.add(number);

        for (Entry<Node, Integer> adjacencyPair : currentNode.adjNodes.entrySet()) {
          Node adjacentNode = adjacencyPair.getKey();
          Integer edgeWeight = adjacencyPair.getValue();

          if (!settledNodes.contains(adjacentNode.number)) {
            int newDistance = currentNode.distance + edgeWeight;
            if (newDistance < adjacentNode.distance) {
              adjacentNode.distance = newDistance;
            }
            pq.add(adjacentNode);
          }
        }
      }
    }
  }

  static class Node implements Comparator<Node> {

    private int number;
    private int distance = Integer.MAX_VALUE;
    private Map<Node, Integer> adjNodes = new HashMap<>();

    public void addDestination(Node des, int distance) {
      adjNodes.put(des, distance);
    }

    public Node(int number) {
      this.number = number;
    }

    public Node() {}

    @Override
    public int compare(Node node, Node node2) {
      return Integer.compare(node.distance, node2.distance);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Node node = (Node) o;
      return Objects.equals(number, node.number);
    }

    @Override
    public int hashCode() {
      return Objects.hash(number);
    }
  }
}
