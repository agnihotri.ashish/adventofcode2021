package com.yoki.advent_of_code.aoc.days2021;

import static com.yoki.advent_of_code.utils.CollectionUtil.asStack;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Optional;
import one.util.streamex.StreamEx;

public class Day18 extends AocDay {

  private final List<String> ip;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day18(String input, PrintStream output) {
    super(input, output);
    this.ip = this.input.lines().toList();
  }

  public String part1() {
    Optional<SnailNumber> reduced = StreamEx.of(ip).map(SnailNumber::new).foldLeft(SnailNumber::new);
    if (reduced.isEmpty()) return "";
    return String.valueOf(reduced.get().magnitude());
  }

  public String part2() {
    List<SnailNumber> snailNumbers = StreamEx.of(ip).map(SnailNumber::new).toList();
    int max = 0;
    for (var sn1 : snailNumbers) {
      for (var sn2 : snailNumbers) {
        if (sn1 != sn2) {
          int newMax = new SnailNumber(new SnailNumber(sn1), new SnailNumber(sn2)).magnitude();
          if (max < newMax) max = newMax;
        }
      }
    }
    return String.valueOf(max);
  }

  static class SnailNumber {

    String num;
    int level;

    SnailNumber leftS;
    Integer left;
    SnailNumber rightS;
    Integer right;

    public SnailNumber(String s) {
      this(s, 0);
    }

    public SnailNumber(String s, int level) {
      num = s;
      this.level = level;
      load();
    }

    public SnailNumber(SnailNumber sn, SnailNumber sn2) {
      this.level = 0;
      leftS = sn;
      rightS = sn2;
      leftS.increment();
      rightS.increment();
      reduce();
    }

    public SnailNumber(SnailNumber sn) {
      this.level = sn.level;
      if (sn.rightS != null)
        this.rightS = new SnailNumber(sn.rightS);
      if (sn.leftS != null)
        this.leftS = new SnailNumber(sn.leftS);
      this.left = sn.left;
      this.right = sn.right;
    }

    public SnailNumber(int left, int right, int level) {
      this("[" + left + "," + right + "]" , level);
    }

    public int magnitude() {
      int l = left != null ? 3 * left : 3 * leftS.magnitude();
      int r = right != null ? 2 * right : 2 * rightS.magnitude();
      return l + r;
    }

    public void increment() {
      this.level++;

      if (leftS != null) {
        leftS.increment();
      }
      if (rightS != null) {
        rightS.increment();
      }
    }

    private void load() {
      pop(); // skip first [
      if (num.charAt(0) == '[') {
        leftS = new SnailNumber(num, level + 1);
        pop(num.length() - leftS.num.length() + 1);
      } else {
        left = Integer.parseInt(skipUntil(","));
      }

      if (num.charAt(0) == '[') {
        rightS = new SnailNumber(num, level + 1);
        pop(num.length() - rightS.num.length() + 1);
      } else {
        right = Integer.parseInt(skipUntil("]"));
      }
    }

    public void reduce() {
      if (explode() || split()) reduce();
    }

    private boolean split() {
      if (left != null && left >= 10) {
        double mid = (double) left / 2;
        leftS = new SnailNumber((int) Math.floor(mid), (int) Math.ceil(mid), level + 1);
        left = null;
        return true;
      }

      if (leftS != null && leftS.split()) return true;

      if (right != null && right >= 10) {
        double mid = (double) right / 2;
        rightS = new SnailNumber((int) Math.floor(mid), (int) Math.ceil(mid), level + 1);
        right = null;
        return true;
      }

      return rightS != null && rightS.split();
    }

    private boolean explode() {
      return this.explode(new ArrayDeque<>());
    }

    private boolean explode(Deque<SnailNumber> previous) {
      if (level == 4 && isEnd()) {
        addLeft(left, asStack(previous));
        addRight(right, asStack(previous));

        var pop = previous.pop();
        if (pop.leftS == this) {
          pop.leftS = null;
          pop.left = 0;
        } else {
          pop.rightS = null;
          pop.right = 0;
        }
        return true;
      }

      if (leftS != null && leftS.explode(asStack(previous, this))) {
        return true;
      }

      return rightS != null && rightS.explode(asStack(previous, this));
    }

    private boolean isEnd() {
      return left != null && right != null;
    }

    private void addLeft(int n, Deque<SnailNumber> previous) {
      SnailNumber pop = this;
      SnailNumber prev;
      while (!previous.isEmpty()) {
        prev = pop;
        pop = previous.pop();
        if (pop.leftS != prev) {
          if (pop.left != null) pop.left += n;
          else {
            var ls = pop.leftS;
            while (ls.rightS != null) {ls = ls.rightS;}
            if (ls.right != null) ls.right += n;
          }
          return;
        }
      }
    }

    private void addRight(int n, Deque<SnailNumber> previous) {
      SnailNumber pop = this;
      SnailNumber prev;
      while (!previous.isEmpty()) {
        prev = pop;
        pop = previous.pop();
        if (pop.rightS != prev) {
          if (pop.right != null) pop.right += n;
          else {
            var rs = pop.rightS;
            while (rs.leftS != null) {rs = rs.leftS;}
            if (rs.left != null) rs.left += n;
          }
          return;
        }
      }
    }

    private String skipUntil(String until) {
      StringBuilder sb = new StringBuilder();
      String c;
      while (!(c = pop()).equals(until)) {sb.append(c);}
      return sb.toString();
    }

    private String pop(int n) {
      String s = num.substring(0, n);
      num = num.substring(n);
      return s;
    }

    private String pop() {
      return pop(1);
    }

    @Override
    public String toString() {
      String l = left != null ? left.toString() : leftS.toString();
      String r = right != null ? right.toString() : rightS.toString();
      return "[" + l + "," + r + "]";
    }
  }
}
