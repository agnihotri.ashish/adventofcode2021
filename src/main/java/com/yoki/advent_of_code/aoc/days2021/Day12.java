package com.yoki.advent_of_code.aoc.days2021;

import static com.yoki.advent_of_code.utils.CollectionUtil.asSet;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.algo.Graph;
import java.io.PrintStream;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

public class Day12 extends AocDay {

  private final List<String> outputValues;
  private final Graph graph;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day12(String input, PrintStream output) {
    super(input, output);
    this.outputValues = this.input.lines().toList();
    this.graph = new Graph();
    outputValues.stream().map(l -> l.split("-")).forEach(s -> graph.add(s[0], s[1]));
  }

  public String part1() {
    return String.valueOf(paths(graph, 1));
  }

  public String part2() {
    return String.valueOf(paths(graph, 2));
  }

  public int paths(Graph graph, int part) {
    return paths(graph, "start", part, new LinkedHashSet<>());
  }

  public int paths(Graph graph, String cave, int part, Set<String> visited) {
    if (cave.equals("end")) return 1;
    if (visited.contains(cave)) {
      if (cave.equals("start")) return 0;
      if (StringUtils.isAllLowerCase(cave)) {
        if (part == 1) return 0;
        else part = 1;
      }
    }

    int finalPart = part;
    return graph.getAdjVertices(cave).stream()
        .mapToInt(n -> paths(graph, n.label(), finalPart, asSet(visited, cave))).sum();
  }
}
