package com.yoki.advent_of_code.aoc.days2021;

import static com.yoki.advent_of_code.utils.BinaryUtil.bin;
import static com.yoki.advent_of_code.utils.BinaryUtil.hexToBin;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import one.util.streamex.LongStreamEx;
import one.util.streamex.StreamEx;

public class Day16 extends AocDay {

  public static final int VERSION = 3;
  public static final int TYPE = 3;
  public static final int LENGTH_TYPE = 1;
  public static final int LT0 = 15;
  public static final int LT1 = 11;
  private final String ip;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day16(String input, PrintStream output) {
    super(input, output);
    this.ip = this.input.strip();
  }

  public String part1() {
    return String.valueOf(packetFactory(hexToBin(ip)).sumVersion());
  }

  public String part2() {
    return String.valueOf(packetFactory(hexToBin(ip)).getValue());
  }

  public static Packet packetFactory(String binary) {
    long type = bin(binary.substring(VERSION, VERSION + TYPE));
    return type == 4 ? new ValuePacket(binary) : operatorFactory(binary);
  }

  public static OperatorPacket operatorFactory(String binary) {
    long lengthTypeId = bin(binary.substring(VERSION + TYPE, VERSION + TYPE + LENGTH_TYPE));
    return lengthTypeId == 0 ? new PacketSizeOperatorPacket(binary) : new PacketNumberOperatorPacket(binary);
  }

  abstract static class Packet {

    String binary;
    long version;
    long type;

    protected Packet(String b) {
      binary = b;
      version = bin(pop(VERSION));
      type = bin(pop(TYPE));
    }

    protected String pop(int n) {
      String s = binary.substring(0, n);
      binary = binary.substring(n);
      return s;
    }

    protected void clearPadding() {
      if (!binary.contains("1")) binary = "";
    }

    abstract long sumVersion();

    abstract long getValue();
  }

  static class ValuePacket extends Packet {

    long value;

    public ValuePacket(String b) {
      super(b);

      StringBuilder fullValue = new StringBuilder();
      boolean cont = true;
      while (cont) {
        cont = bin(pop(1)) != 0;
        fullValue.append(pop(4));
      }
      clearPadding();
      value = bin(fullValue.toString());
    }

    @Override
    long sumVersion() {
      return version;
    }

    @Override
    long getValue() {
      return value;
    }
  }

  abstract static class OperatorPacket extends Packet {

    long lengthTypeId;
    List<Packet> packets;

    protected OperatorPacket(String b) {
      super(b);
      lengthTypeId = bin(pop(LENGTH_TYPE));
    }

    protected void addPackage() {
      Packet p = packetFactory(binary);
      packets.add(p);
      pop(binary.length() - p.binary.length());
    }

    @Override
    long sumVersion() {
      return this.version + StreamEx.of(packets).mapToLong(Packet::sumVersion).sum();
    }

    @Override
    long getValue() {
      if (packets.size() == 1) return packets.get(0).getValue();

      LongStreamEx valueStream = StreamEx.of(packets).mapToLong(Packet::getValue);
      if (type == 0L)
        return valueStream.sum();

      if (type == 1L)
        return valueStream.reduce(1, (a, b) -> a * b);

      if (type == 2L)
        return valueStream.min().orElse(0);

      if (type == 3L)
        return valueStream.max().orElse(0);

      if (type == 5L)
        return packets.get(0).getValue() > packets.get(1).getValue() ? 1L : 0L;

      if (type == 6L)
        return packets.get(0).getValue() < packets.get(1).getValue() ? 1L : 0L;

      if (type == 7L)
        return packets.get(0).getValue() == packets.get(1).getValue() ? 1L : 0L;

      return 0;
    }
  }

  static class PacketSizeOperatorPacket extends OperatorPacket {

    public PacketSizeOperatorPacket(String b) {
      super(b);
      long totalSize = bin(pop(LT0));
      packets = new ArrayList<>();
      int init = binary.length();
      while (init - binary.length() < totalSize) {addPackage();}
      clearPadding();
    }
  }

  static class PacketNumberOperatorPacket extends OperatorPacket {

    public PacketNumberOperatorPacket(String b) {
      super(b);
      long nPackets = bin(pop(LT1));
      packets = new ArrayList<>();
      for (int i = 0; i < nPackets; i++) addPackage();
      clearPadding();
    }
  }
}
