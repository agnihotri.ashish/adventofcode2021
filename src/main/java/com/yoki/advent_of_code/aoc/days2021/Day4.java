package com.yoki.advent_of_code.aoc.days2021;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.Board;
import com.yoki.advent_of_code.utils.Board.Case;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import one.util.streamex.StreamEx;

public class Day4 extends AocDay {

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day4(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    var inputs = this.input.lines().collect(Collectors.toList());
    var commands = StreamEx.split(inputs.remove(0), ",").map(Integer::parseInt).toList();
    inputs.remove(0);
    var boards = loadBoards(inputs);
    return String.valueOf(getWinner(commands, boards));
  }

  public String part2() {
    var inputs = this.input.lines().collect(Collectors.toList());
    var commands = StreamEx.split(inputs.remove(0), ",").map(Integer::parseInt).toList();
    inputs.remove(0);
    var boards = loadBoards(inputs);
    return String.valueOf(getLastWinner(commands, boards));
  }

  private List<Board> loadBoards(List<String> lines) {
    List<Board> boards = new ArrayList<>();
    List<List<Case>> board = new ArrayList<>();
    List<String> linesCopy = new ArrayList<>(lines);
    for (String line : linesCopy) {
      if (line.isBlank()) {
        boards.add(new Board(board));
        board = new ArrayList<>();
      } else {
        board.add(Arrays.stream(line.replace("  ", " ").strip().split(" "))
            .map(Integer::parseInt).map(Case::new).toList());
      }
    }
    boards.add(new Board(board));
    return boards;
  }

  private int getWinner(List<Integer> commands, List<Board> boards) {
    for (var n : commands) {
      for (var b : boards) {
        b.check(n);
        if (b.getSuccess()) return b.getScore(n);
      }
    }
    return 0;
  }

  private int getLastWinner(List<Integer> commands, List<Board> boards) {
    int success = 0;
    for (var n : commands) {
      for (var b : boards) {
        b.check(n);
        if (!b.getSuccess() && b.isSuccess()) {
          int score = b.getScore(n);
          success += 1;
          if (success == boards.size()) return score;
        }
      }
    }
    return 0;
  }
}
