package com.yoki.advent_of_code.aoc.days2021;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Day24 extends AocDay {

  private final List<String> ip;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day24(String input, PrintStream output) {
    super(input, output);
    this.ip = this.input.lines().toList();
  }

  public String part1() {
    Optional<Long> res = Optional.empty();
    for (long i = 9999999L; i >= 1111111L; i--) {
      res = optimizedProcess(i);
      if (!String.valueOf(i).contains("0") && res.isPresent()) break;
    }

    if (res.isPresent()) {
      boolean check = processMonad(ip, res.get());
      return res.get() + " : " + check;
    }
    return "";
  }

  public String part2() {
    Optional<Long> res2 = Optional.empty();
    for (long i = 1111111L; i < 9999999L; i++) {
      res2 = optimizedProcess(i);
      if (!String.valueOf(i).contains("0") && res2.isPresent())
        break;
    }

    if (res2.isPresent()) {
      boolean check = processMonad(ip, res2.get());
      return res2.get() + " : " + check;
    }
    return "";
  }

  private Optional<Long> optimizedProcess(long digits) {
    int[] cGroup = {2, 4, 8, 7, 12, 7, 10, 14, 2, 6, 8, 11, 5, 11};
    int[] required = {10, 10, 14, 11, 14, -14, 0, 10, -10, 13, -12, -3, -11, -2};
    boolean[] type = {true, true, true, true, true, false, false, true, false, true, false, false, false, false};

    int z = 0;
    int iDigit = 0;
    String input = String.valueOf(digits);

    int[] res = new int[14];

    for (int i = 0; i < 14; i++) {
      if (type[i]) {
        int w = Integer.parseInt(String.valueOf(input.charAt(iDigit)));
        z = (26*z)+w+cGroup[i];
        res[i] = w;
        iDigit++;
      } else {
        res[i] = (z%26) + required[i];
        z /=26;
        if (res[i] > 9 || res[i] < 1) return Optional.empty();
      }
    }
    return Optional.of(Long.parseLong(Arrays.stream(res)
        .mapToObj(String::valueOf).collect(Collectors.joining())));
  }

  private boolean processMonad(List<String> ip, long nInput) {
    Map<Character, Long> rs = new HashMap<>(Map.of('w', 0L, 'x', 0L, 'y', 0L, 'z', 0L));
    String input = String.valueOf(nInput);
    int i = 0;
    for (var line : ip) {
      String[] s = line.split(" ");
      char reg = toReg(s[1]);
      if (s.length == 2) {
        rs.put(reg, Long.parseLong(String.valueOf(input.charAt(i))));
        i++;
      } else {
        rs.compute(reg, (k, v) -> execute(s[0], v, valueOf(s[2], rs)));
      }
    }
    return rs.get('z') == 0;
  }

  private long execute(String op, long r1, long arg) {
    switch (op) {
      case "add" -> r1 += arg;
      case "mul" -> r1 *= arg;
      case "div" -> r1 /= arg;
      case "mod" -> r1 %= arg;
      case "eql" -> r1 = r1 == arg ? 1 : 0;
      default -> throw new IllegalStateException("Unexpected value: " + op);
    }
    return r1;
  }

  private long valueOf(String s, Map<Character, Long> rs) {
    try {
      return Long.parseLong(s);
    } catch (NumberFormatException ignored) {
      return rs.get(toReg(s));
    }
  }

  private char toReg(String s) {
    return s.charAt(0);
  }
}
