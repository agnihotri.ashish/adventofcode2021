package com.yoki.advent_of_code.aoc.days2021;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Optional;

public class Day17 extends AocDay {

  private int maxH;
  private int n;
  private final int xs;
  private final int xe;
  private final int ys;
  private final int ye;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day17(String input, PrintStream output) {
    super(input, output);
    xs = 85;
    xe = 145;
    ys = -108;
    ye = -163;

    this.maxH = 0;
    this.n = 0;
    for (int i = 0; i <= xe; i++) {
      for (int j = ye; j <= -ye; j++) {
        Optional<Probe> sim = simulate(i, j);
        if (sim.isPresent()) {
          maxH = Math.max(maxH, sim.get().maxY);
          n++;
        }
      }
    }

  }

  public String part1() {
    return String.valueOf(maxH);
  }

  public String part2() {
    return String.valueOf(n);
  }

  Optional<Probe> simulate(int vx, int vy) {
    Probe probe = new Probe(vx, vy);
    while (!probe.fail(xe, ye)) {
      probe.move();
      if (probe.inArea(xs, xe, ys, ye)) return Optional.of(probe);
    }
    return Optional.empty();
  }

  static class Probe {

    int x = 0;
    int y = 0;
    int initVX;
    int initVY;
    int vy;
    int vx;
    int maxY = 0;

    public Probe(int vx, int vy) {
      initVX = vx;
      initVY = vy;
      this.vx = vx;
      this.vy = vy;
    }

    public void move() {
      x += vx;
      y += vy;
      if (vx > 0) vx--;
      if (vx < 0) vx++;
      vy--;

      maxY = Math.max(maxY, y);
    }

    public boolean inArea(int xs, int xe, int ys, int ye) {
      // 20 30 -5 -10
      return xs <= x && x <= xe && ys >= y && y >= ye;
    }

    public boolean fail(int xe, int ye) {
      return x > xe || y < ye;
    }

    @Override
    public String toString() {
      return "Probe{vx=" + initVX + ", vy=" + initVY + "}";
    }
  }
}
