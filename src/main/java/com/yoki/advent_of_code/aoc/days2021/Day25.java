package com.yoki.advent_of_code.aoc.days2021;

import static java.util.stream.Collectors.toList;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Arrays;

public class Day25 extends AocDay {

  private final int size;
  private final int size2;
  private final char[][] map;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day25(String input, PrintStream output) {
    super(input, output);
    var outputValues = this.input.lines().map(l -> l.chars().mapToObj(c -> (char)c).collect(toList())).collect(toList());

    this.size = outputValues.size();
    this.size2 = outputValues.get(0).size();
    this.map = new char[size][size2];
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size2; j++) {
        map[i][j] = outputValues.get(i).get(j);
      }
    }
  }

  public String part1() {
    SeaFloor sf = new SeaFloor(map);
    int i;
    for (i=1;sf.step();i++);
    return String.valueOf(i);
  }

  public String part2() {
    return String.valueOf(0);
  }

  enum CucumberType {
    EAST, SOUTH, NONE;

    public static CucumberType fromChar(char c) {
      if (c == '>')
        return EAST;
      if (c == 'v')
        return SOUTH;
      return NONE;
    }

    public boolean empty() {
      return this == NONE;
    }

    @Override
    public String toString() {
      if (this == EAST) return ">";
      if (this == SOUTH) return "v";
      return ".";
    }
  }

  static class SeaFloor {

    int xLen;
    int yLen;
    CucumberType[][] cucumbers;

    public SeaFloor(char[][] ip) {
      xLen = ip.length;
      yLen = ip[0].length;
      cucumbers = new CucumberType[xLen][yLen];
      for (int i = 0; i < xLen; i++) {
        for (int j = 0; j < yLen; j++) {
          cucumbers[i][j] = CucumberType.fromChar(ip[i][j]);
        }
      }
    }

    public boolean step() {
      return moveEasts() | moveSouths();
    }

    private boolean moveEasts() {
      boolean moved = false;
      CucumberType[][] afterEast = new CucumberType[xLen][yLen];
      for (int i = 0; i < xLen; i++) {
        for (int j = 0; j < yLen; j++) {
          int newY = (j + 1) % yLen;
          if (cucumbers[i][j] == CucumberType.EAST && cucumbers[i][newY].empty()) {
            afterEast[i][newY] = cucumbers[i][j];
            afterEast[i][j] = cucumbers[i][newY];
            moved = true;
          }
        }
      }
      if (moved) replaceMap(afterEast, cucumbers);
      return moved;
    }

    private boolean moveSouths() {
      boolean moved = false;
      CucumberType[][] afterSouth = new CucumberType[xLen][yLen];
      for (int i = 0; i < xLen; i++) {
        for (int j = 0; j < yLen; j++) {
          int newX = (i + 1) % xLen;
          if (cucumbers[i][j] == CucumberType.SOUTH && cucumbers[newX][j].empty()) {
            afterSouth[newX][j] = cucumbers[i][j];
            afterSouth[i][j] = cucumbers[newX][j];
            moved = true;
          }
        }
      }
      if (moved) replaceMap(afterSouth, cucumbers);
      return moved;
    }

    public void replaceMap(CucumberType[][] from, CucumberType[][] to) {
      for (int i = 0; i < xLen; i++)
        for (int j = 0; j < yLen; j++)
          if (from[i][j] != null) to[i][j] = from[i][j];
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      for (CucumberType[] c : cucumbers) {
        sb.append(Arrays.toString(c));
        sb.append("\n");
      }
      return sb.toString();
    }
  }
}
