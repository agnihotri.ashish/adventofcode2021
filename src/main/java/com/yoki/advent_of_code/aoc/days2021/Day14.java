package com.yoki.advent_of_code.aoc.days2021;

import static com.yoki.advent_of_code.utils.StringUtil.stringToChars;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.data_structure.Counter;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import one.util.streamex.StreamEx;

public class Day14 extends AocDay {

  private final List<String> ip;
  private final PolymersSequence poly;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day14(String input, PrintStream output) {
    super(input, output);
    this.ip = this.input.lines().toList();
    this.poly = new PolymersSequence(ip);
  }

  public String part1() {
    return String.valueOf(poly.continueSequence(10).result());
  }

  public String part2() {
    return String.valueOf(poly.continueSequence(30).result());
  }


  private static String join(Character c1, Character c2) {
    return "" + c1 + c2;
  }

  static class PolymersSequence {

    private final Map<String, Character> polymersRules;
    private final Counter<Character> chars;
    private final Counter<String> pairs;

    public PolymersSequence(List<String> inputs) {
      List<Character> characters = stringToChars(inputs.get(0));
      chars = new Counter<>(characters);
      pairs = new Counter<>(makePairs(characters));
      polymersRules = getPolymers(inputs);
    }

    public long result() {
      return chars.max() - chars.min();
    }

    public PolymersSequence continueSequence(int n) {
      for (int i = 0; i < n; i++) oneStep();
      return this;
    }

    private void oneStep() {
      for (var e : new HashMap<>(pairs).entrySet()) {
        String pair = e.getKey();
        Long count = e.getValue();

        char left = pair.charAt(0);
        char right = pair.charAt(1);
        char toInsert = polymersRules.get(pair);

        pairs.decrement(pair, count);
        pairs.increment(join(left, toInsert), count);
        pairs.increment(join(toInsert, right), count);
        chars.increment(toInsert, count);
      }
    }

    private static Map<String, Character> getPolymers(List<String> outputValues) {
      return StreamEx.of(outputValues).skip(2).map(i -> i.split(" -> ")).toMap(i -> i[0], i -> i[1].charAt(0));
    }

    private static List<String> makePairs(List<Character> characters) {
      return StreamEx.of(characters).pairMap(Day14::join).toList();
    }
  }
}
