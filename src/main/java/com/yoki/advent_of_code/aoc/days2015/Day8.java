package com.yoki.advent_of_code.aoc.days2015;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Day8 extends AocDay {

  private final List<String> ip;
  private final AtomicInteger stringSize;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day8(String input, PrintStream output) {
    super(input, output);
    this.ip = this.input.lines().toList();
    this.stringSize = new AtomicInteger(0);
  }

  public String part1() {
    int memorySize = ip.stream()
        .peek(x -> stringSize.set(stringSize.addAndGet(x.length())))
        .map(x -> x.replace("\\\\", "S"))
        .map(x -> x.replace("\\\"", "Q"))
        .map(x -> x.replace("\"", ""))
        .map(x -> x.replaceAll("\\\\x[0-9a-f]{2}", "X"))
        .mapToInt(String::length).sum();
    return String.valueOf(stringSize.get() - memorySize); // 1371

  }

  public String part2() {
    int embiggen = ip.stream()
        .map(x -> x.replaceAll("\\\\x[0-9a-f]{2}", "XXXXX"))
        .map(x -> x.replace("\\\"", "QQQQ"))
        .map(x -> x.replace("\\\\", "SSSS"))
        .mapToInt(x -> x.length() + 4).sum();
    return String.valueOf(embiggen - stringSize.get());
  }
}