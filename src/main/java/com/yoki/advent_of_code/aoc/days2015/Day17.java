package com.yoki.advent_of_code.aoc.days2015;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import one.util.streamex.IntStreamEx;
import org.paukov.combinatorics3.Generator;

public class Day17 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day17(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    int total = 150;
    List<Integer> containers = this.input.lines().map(Integer::parseInt).toList();

    List<List<Integer>> combinations = IntStreamEx.range(1, containers.size())
        .flatMapToObj(r -> Generator.combination(containers)
            .simple(r)
            .stream()
            .filter(p -> p.stream().mapToInt(i -> i).sum() == total))
        .toList();

    return String.valueOf(combinations.size());
  }

  public String part2() {
    int total = 150;
    List<Integer> containers = this.input.lines().map(Integer::parseInt).toList();
    List<List<Integer>> res = calculateMinimumContainerSize(total, containers);
    return String.valueOf(res.size());
  }

  private List<List<Integer>> calculateMinimumContainerSize(int total, List<Integer> containers) {
    for (int i = 0; i < containers.size(); i++) {
      List<List<Integer>> res = generateCombinations(total, containers, i);
      if (!res.isEmpty()) return res;
    }
    return new ArrayList<>();
  }

  private List<List<Integer>> generateCombinations(int total, List<Integer> containers, int r) {
    return Generator.combination(containers)
        .simple(r)
        .stream()
        .filter(p -> p.stream().mapToInt(i -> i).sum() == total)
        .toList();
  }
}