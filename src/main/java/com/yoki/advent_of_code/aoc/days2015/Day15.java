package com.yoki.advent_of_code.aoc.days2015;

import static java.lang.Integer.parseInt;
import static java.lang.Math.max;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class Day15 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day15(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    List<Ingredient> ingredients = this.input.lines().map(Ingredient::toIngredient).toList();
    return String.valueOf(getMaximumDosages(ingredients));
  }

  public String part2() {
    return String.valueOf(0);
  }

  private List<int[]> possibilities(int nLoop) {
    return possibilities(nLoop, 0, new int[nLoop], new ArrayList<>());
  }

  private List<int[]> possibilities(int n, int v, int[] indices, List<int[]> pos) {
    var res = new ArrayList<>(pos);
    if (n > 0) {
      for (int i = 1; i < 100; i++) {
        indices[n - 1] = i;
        res.addAll(possibilities(n - 1, v + i, indices, pos));
      }
    }
    if (n == 0 && v == 100) res.add(indices.clone());
    return res;
  }

  private int getMaximumDosages(List<Ingredient> igs) {
    int max = 0;
    for (var pos : possibilities(igs.size())) {
      Ingredient tot = new Ingredient(0, 0, 0, 0, 0);
      for (int i = 0; i < igs.size(); i++) {
        tot = tot.add(igs.get(i).mul(pos[i]));
      }
      if (tot.calories == 500)
        max = max(max, tot.total());
    }
    return max;
  }


  record Ingredient(int capacity, int durability, int flavor, int texture, int calories) {

    public static Ingredient toIngredient(String line) {
      String[] split = line.replace(",", "").split(" ");
      return new Ingredient(parseInt(split[2]), parseInt(split[4]), parseInt(split[6]), parseInt(split[8]), parseInt(split[10]));
    }

    private int mul(int m, int v) {
      return m * v;
    }

    public Ingredient mul(int v) {
      return new Ingredient(mul(capacity, v), mul(durability, v), mul(flavor, v), mul(texture, v), mul(calories, v));
    }

    public Ingredient add(Ingredient o) {
      return new Ingredient(capacity + o.capacity, durability + o.durability, flavor + o.flavor, texture + o.texture,
          calories + o.calories);
    }

    public int total() {
      return max(0, capacity) * max(0, durability) * max(0, flavor) * max(0, texture);
    }
  }
}