package com.yoki.advent_of_code.aoc.days2015;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import org.apache.commons.codec.digest.DigestUtils;

public class Day4 extends AocDay {

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day4(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    return String.valueOf(bruteForceMD5(this.input.trim(), 5));
  }

  public String part2() {
    return String.valueOf(bruteForceMD5(this.input.trim(), 6));
  }

  public int bruteForceMD5(String prefix, int nZeros) {
    int i = 0;
    while (true) {
      String hash = DigestUtils.md5Hex(prefix + i);
      if (hash.startsWith("0".repeat(nZeros))) return i;
      i++;
    }
  }
}