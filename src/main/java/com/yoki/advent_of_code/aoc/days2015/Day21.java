package com.yoki.advent_of_code.aoc.days2015;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import one.util.streamex.IntStreamEx;

public class Day21 extends AocDay {

  Random rand = SecureRandom.getInstanceStrong();

  public static final List<Equipment> SHOP = List.of(
      new Equipment("Dagger", 8, 4, 0, EquipmentType.WEAPON),
      new Equipment("Shortsword", 10, 5, 0, EquipmentType.WEAPON),
      new Equipment("Warhammer", 25, 6, 0, EquipmentType.WEAPON),
      new Equipment("Longsword", 40, 7, 0, EquipmentType.WEAPON),
      new Equipment("Greataxe", 74, 8, 0, EquipmentType.WEAPON),

      new Equipment("Leather", 13, 0, 1, EquipmentType.ARMOR),
      new Equipment("Chainmail", 31, 0, 2, EquipmentType.ARMOR),
      new Equipment("Splintmail", 53, 0, 3, EquipmentType.ARMOR),
      new Equipment("Bandedmail", 75, 0, 4, EquipmentType.ARMOR),
      new Equipment("Platemail", 102, 0, 5, EquipmentType.ARMOR),

      new Equipment("Damage +1", 25, 1, 0, EquipmentType.RING),
      new Equipment("Damage +2", 50, 2, 0, EquipmentType.RING),
      new Equipment("Damage +3", 100, 3, 0, EquipmentType.RING),
      new Equipment("Defense +1", 20, 0, 1, EquipmentType.RING),
      new Equipment("Defense +2", 40, 0, 2, EquipmentType.RING),
      new Equipment("Defense +3", 80, 0, 3, EquipmentType.RING)
  );

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day21(String input, PrintStream output) throws NoSuchAlgorithmException {
    super(input, output);
  }

  public String part1() {
    return String.valueOf(cheapestEquipment());
  }

  public String part2() {
    return String.valueOf(highestBossEquipment());
  }

  private int highestBossEquipment() {
    return IntStreamEx.range(100000).map(i -> winningEquipment(false)).max().orElse(-1);
  }

  private int cheapestEquipment() {
    return IntStreamEx.range(100000).map(i -> winningEquipment(true)).min().orElse(-1);
  }

  private int winningEquipment(boolean cheapest) {
    Optional<Integer> res;
    while ((res = simulateCheapestEquipment(cheapest)).isEmpty()) {}
    return res.get();
  }

  @SneakyThrows
  private Optional<Integer> simulateCheapestEquipment(boolean cheapest) {
    EquipmentsSystem system = new EquipmentSelector(SHOP, this.rand).pickEquipments();
    Entity player = new Entity("Player", 100, system);
    Entity enemy = Entity.fromInput(this.input);
    return new Game(player, enemy).play() == cheapest ? Optional.of(system.totalCost()) : Optional.empty();
  }

  static class Game {

    Entity player;
    Entity enemy;

    public Game(Entity player, Entity enemy) {
      this.player = player;
      this.enemy = enemy;
    }

    public boolean play() {
      Deque<Entity> entities = new ArrayDeque<>(List.of(this.player, this.enemy));
      while (entities.stream().noneMatch(Entity::dead)) {
        Entity pop = entities.remove();
        pop.attack(Objects.requireNonNull(entities.peek()));
        entities.add(pop);
      }
      return this.enemy.dead();
    }
  }

  /**
   * Equipment
   **/

  static class EquipmentSelector {

    private final EquipmentsSystem system;
    private final List<Equipment> store;
    private final Random rand;

    public EquipmentSelector(List<Equipment> store, Random rand) {
      this.system = new EquipmentsSystem();
      this.rand = rand;
      this.store = store;
    }

    public EquipmentsSystem pickEquipments() {
      this.system.slots.forEach(this::pickRandEquip);
      return this.system;
    }

    private void pickRandEquip(Slot slot) {
      if (!slot.required() && this.rand.nextBoolean()) return;
      List<Equipment> filtered = store.stream().filter(e -> e.type().equals(slot.type())).toList();
      while (!this.system.addEquipment(filtered.get(this.rand.nextInt(filtered.size())))) {}
    }
  }


  enum EquipmentType {
    WEAPON, ARMOR, RING
  }

  static record Equipment(String name, int cost, int damage, int armor, EquipmentType type) {
  }

  /**
   * DAMAGE SYSTEM
   **/

  interface DamageSystem {

    int damage();

    int armor();
  }

  static record Slot(EquipmentType type, boolean required) {}

  @Data
  static class EquipmentsSystem implements DamageSystem {

    private List<Slot> slots = List.of(
        new Slot(EquipmentType.WEAPON, true),
        new Slot(EquipmentType.ARMOR, false),
        new Slot(EquipmentType.RING, false),
        new Slot(EquipmentType.RING, false));

    private Set<Equipment> equipments = new HashSet<>();

    public boolean addEquipment(Equipment equipment) {
      return equipments.add(equipment);
    }

    @Override
    public int damage() {
      return this.equipments.stream().mapToInt(Equipment::damage).sum();
    }

    @Override
    public int armor() {
      return this.equipments.stream().mapToInt(Equipment::armor).sum();
    }

    public int totalCost() {
      return this.equipments.stream().mapToInt(Equipment::cost).sum();
    }
  }

  @Data
  @AllArgsConstructor
  static class RawSystem implements DamageSystem {

    private int damage;
    private int armor;

    @Override
    public int damage() {
      return this.damage;
    }

    @Override
    public int armor() {
      return this.armor;
    }
  }


  /**
   * ENTITY
   **/

  @Data
  @AllArgsConstructor
  static class Entity {

    private String name;
    private int life;
    private DamageSystem system;

    public static Entity fromInput(String input) {
      int[] ints = input.lines().map(i -> i.split(": ")[1]).mapToInt(Integer::parseInt).toArray();
      return new Entity("Enemy", ints[0], new RawSystem(ints[1], ints[2]));
    }

    public void attack(Entity other) {
      other.hit(this.system.damage() - other.system.armor());
    }

    public void hit(int damage) {
      this.life -= damage > 0 ? damage : 1;
    }

    public boolean dead() {
      return this.life <= 0;
    }
  }
}