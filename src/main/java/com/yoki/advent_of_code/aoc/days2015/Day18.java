package com.yoki.advent_of_code.aoc.days2015;

import static java.util.stream.Collectors.toList;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

public class Day18 extends AocDay {

  protected static final int[][] NEIGHBOURS = {
      {-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}
  };

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day18(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    YardLights yard = new YardLights(this.input.lines().collect(toList()));
    yard.step(100);
    return String.valueOf(yard.totalLights());
  }

  public String part2() {
    return part1();
  }

  static class YardLights {

    Boolean[][] lights;

    public YardLights(List<String> lines) {
      this.lights = lines.stream()
          .map(l -> Arrays.stream(l.split(""))
              .map(c -> c.equals("#")).toArray(Boolean[]::new))
          .toArray(Boolean[][]::new);
      // part 2
      this.lights[0][0] = true;
      this.lights[0][lights.length-1] = true;
      this.lights[lights.length-1][0] = true;
      this.lights[lights.length-1][lights.length-1] = true;
    }

    public void step(int n) {
      for (int i = 0; i < n; i++) step();
    }

    public void step() {
      Boolean[][] light = new Boolean[lights.length][lights.length];
      for (int i = 0; i < light.length; i++) {
        for (int j = 0; j < light.length; j++) {
          light[i][j] = computeNewState(i, j);
        }
      }
      this.lights = light;
    }

    private boolean computeNewState(int i, int j) {
      long nLightOn = Arrays.stream(NEIGHBOURS)
          .filter(n -> this.getLight(i + n[0], j + n[1]))
          .count();

      // part 2
      if ((i == 0 && j == 0) || (i == lights.length - 1 && j == lights.length - 1)) return true;
      if ((i == 0 && j == lights.length - 1) || (i == lights.length - 1 && j == 0)) return true;

      if (getLight(i, j) && (nLightOn == 2 || nLightOn == 3)) return true;
      else return !getLight(i, j) && nLightOn == 3;
    }

    private boolean getLight(int i, int j) {
      if (i < 0 || i >= lights.length) return false;
      if (j < 0 || j >= lights.length) return false;
      return this.lights[i][j];
    }

    @Override
    public String toString() {
      StringBuilder res = new StringBuilder();
      for (var b : lights) {
        for (var l : b) {
          res.append(l ? "#" : ".");
        }
        res.append("\n");
      }
      return res.toString();
    }

    public long totalLights() {
      return Arrays.stream(lights).mapToInt(b -> (int) Arrays.stream(b).filter(a -> a).count()).sum();
    }
  }
}