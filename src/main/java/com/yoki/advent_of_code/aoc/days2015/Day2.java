package com.yoki.advent_of_code.aoc.days2015;

import static org.apache.commons.lang3.math.NumberUtils.max;
import static org.apache.commons.lang3.math.NumberUtils.min;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;

public class Day2 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day2(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    List<String> ip = this.input.lines().toList();
    int res = ip.stream().map(Gift::new).mapToInt(Gift::wrapping).sum();
    return String.valueOf(res);
  }

  public String part2() {
    List<String> ip = this.input.lines().toList();
    int res = ip.stream().map(Gift::new).mapToInt(Gift::fullRibon).sum();
    return String.valueOf(res);
  }

  record Gift(int l, int w, int h) {

    public Gift(String l) {
      this(
          Integer.parseInt(l.split("x")[0]),
          Integer.parseInt(l.split("x")[1]),
          Integer.parseInt(l.split("x")[2])
      );
    }

    public int fullRibon() {
      return ribonLength() + area();
    }

    public int ribonLength() {
      int max = max(l, w, h);
      if (max == l) return h*2 + w*2;
      if (max == w) return l*2 + h*2;
      return l*2 + w*2;
    }

    public int wrapping() {
      return surface() + smallestSurface();
    }

    private int area() {
      return l*w*h;
    }

    private int surface() {
      return 2 * l * w + 2 * w * h + 2 * h * l;
    }

    private int smallestSurface() {
      return min(l * w, w * h, h * l);
    }
  }
}