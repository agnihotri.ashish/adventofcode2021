package com.yoki.advent_of_code.aoc.days2015;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day10 extends AocDay {


  private final List<Integer> ints;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day10(String input, PrintStream output) {
    super(input, output);
    this.ints = Arrays.stream(this.input.strip().split("")).map(Integer::parseInt).toList();
  }

  public String part1() {
    int n = 40;
    List<Integer> intList = this.ints;
    for (int i = 0; i < n; i++) {
      intList = lookAndSay(intList);
    }
    return String.valueOf(intList.size());
  }

  public String part2() {
    int n = 50;
    List<Integer> intList = this.ints;
    for (int i = 0; i < n; i++) {
      intList = lookAndSay(intList);
    }
    return String.valueOf(intList.size());
  }

  private List<Integer> lookAndSay(List<Integer> ints) {
    List<Integer> res = new ArrayList<>();
    int count = 1;
    for (int i = 0; i < ints.size()-1; i++) {
      Integer current = ints.get(i);
      Integer next = ints.get(i + 1);
      if (current.equals(next)) {
        count++;
        continue;
      }
      res.add(count);
      res.add(current);
      count = 1;
    }
    res.add(count);
    res.add(ints.get(ints.size()-1));
    return res;
  }
}