package com.yoki.advent_of_code.aoc.days2015;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Data;

public class Day22 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day22(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    return String.valueOf(0);
  }

  public String part2() {
    return String.valueOf(0);
  }

  static class Game {

    Entity player;
    Entity enemy;

    public Game(Entity player, Entity enemy) {
      this.player = player;
      this.enemy = enemy;
    }

    public boolean play() {
      Deque<Entity> entities = new ArrayDeque<>(List.of(this.player, this.enemy));
      while (entities.stream().noneMatch(Entity::dead)) {
        Entity pop = entities.remove();
        pop.attack(Objects.requireNonNull(entities.peek()));
        entities.add(pop);
      }
      return this.enemy.dead();
    }
  }


  /**
   *  MAGIC
   **/

  interface Effect {

  }

  /**
   * DAMAGE SYSTEM
   **/

  interface DamageSystem {

    int damage();

    int armor();
  }

  @Data
  @AllArgsConstructor
  static class MagicSystem implements DamageSystem {

    private int mana;

    @Override
    public int damage() {
      return 0;
    }

    @Override
    public int armor() {
      return 0;
    }
  }


  @Data
  @AllArgsConstructor
  static class RawSystem implements DamageSystem {

    private int damage;
    private int armor;

    @Override
    public int damage() {
      return this.damage;
    }

    @Override
    public int armor() {
      return this.armor;
    }
  }

  /**
   * ENTITY
   **/

  @Data
  static class Entity {

    private String name;
    private int life;
    private DamageSystem system;

    private List<Effect> effects = new ArrayList<>();

    public Entity(String name, int life, DamageSystem system) {
      this.name = name;
      this.life = life;
      this.system = system;
    }

    public static Entity fromInput(String input) {
      int[] ints = input.lines().map(i -> i.split(": ")[1]).mapToInt(Integer::parseInt).toArray();
      return new Entity("Enemy", ints[0], new RawSystem(ints[1], 0));
    }

    public void attack(Entity other) {
      other.hit(this.system.damage());
    }

    public void hit(int damage) {
      this.life -= damage > 0 ? damage : 1;
    }

    public boolean dead() {
      return this.life <= 0;
    }
  }
}