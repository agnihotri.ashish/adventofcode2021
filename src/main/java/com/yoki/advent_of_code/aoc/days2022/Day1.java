package com.yoki.advent_of_code.aoc.days2022;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import one.util.streamex.IntStreamEx;

public class Day1 extends AocDay {

  public static final String EMPTY_ENTRY = "\n\n";

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day1(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    List<Integer> caloriesByElf = getCalories(this.input);
    return String.valueOf(IntStreamEx.of(caloriesByElf).max().orElse(-1));
  }

  public String part2() {
    List<Integer> caloriesByElf = getCalories(this.input);
    return String.valueOf(IntStreamEx.of(caloriesByElf).reverseSorted().limit(3).sum());
  }

  private List<Integer> getCalories(String inventory) {
    return Arrays.stream(inventory.split(EMPTY_ENTRY))
        .map(s -> s.lines().mapToInt(Integer::parseInt).sum())
        .toList();
  }
}
