package com.yoki.advent_of_code.mit_Algos;

public class Lecture2 {

  public static void main(String[] args) {
    // Random access machine / Random access memory (RAM)

    // Document Distance Problem
    // d(D1,D2)
    // Document = Sequence of words
    // Word = String of alpha (a-z,0-9)

    // Idea: look at shared words

    // think of document as a vector
    // D[w] = # occurrences of w in D

    // D1 = "The cat"
    // D2 = "The dog"

    // CAT    D1    THE     D2    DOG

    // dot() product of D1.D2

    String d1 = "the cat";
    String d2 = "the dog";
  }
}
