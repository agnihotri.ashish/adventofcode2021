package com.yoki.advent_of_code.mit_Algos;

import static com.yoki.advent_of_code.aoc.AoC.NS_TO_MS;
import static com.yoki.advent_of_code.utils.tailrecursion.TailCall.done;

import com.yoki.advent_of_code.utils.CollectionUtil;
import com.yoki.advent_of_code.utils.tailrecursion.TailCall;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.function.Consumer;
import one.util.streamex.IntStreamEx;
import org.apache.commons.lang3.tuple.Pair;

public class Lecture1 {

  public static void main(String[] args) {
    peekFinderOneDim();
//    peekFinderTwoDim();
  }

  private static void peekFinderTwoDim() {
    // Peek Finder Two Dimension
    // int[][]
    // n rows, n columns
    // a is a 2D-peek iff a>=b, a>=d, a>=c, a>=e

    // Greedy Ascent Algorithm: O(n * m)
    // [14,13,12]
    // [15, 9,11]
    // [16,17,20]

    // Pick a middle column j = m/2
    // Find global max on column j at (i,j)
    // compare (i,j-1), (i,j), (i,j+1)
    // pick the left cols if (i,i-1) > (i,j)
    // pick the right cold if (i,i+1) > (i,j)
    // else then we found it
    find2dPeek(new int[][]{
        {1,2,3},
        {4,5,6},
        {7,8,9}}
    ).ifPresent(System.out::println);

    find2dPeek(new int[][]{
        {2,2,2},
        {2,5,2},
        {2,2,2}}
    ).ifPresent(System.out::println);

    find2dPeek(new int[][]{
        {4,3,4},
        {5,2,5},
        {6,3,2}}
    ).ifPresent(System.out::println);
  }

  // T(n,m) = T(n,m/2) + O(n) <- max
  // T(n,1) = O(n)
  // T(n,m) = O(n) + ... + O(n) = O(n log2 m)
  public static Optional<Pair<Integer, Integer>> find2dPeek(int[][] ip) {
    return find2dPeek(ip, 0, ip.length-1);
  }

  private static Optional<Pair<Integer, Integer>> find2dPeek(int[][] ip, int m, int n) {
    int i = (m + n) / 2;
    int j = findPeekR(ip[i]).get();
    if (i > 0 && ip[i][j] < ip[i-1][j]) return find2dPeek(ip, m, i - 1);
    if (i < ip.length - 1 && ip[i][j] < ip[i + 1][j]) return find2dPeek(ip, i + 1, n);
    return Optional.of(Pair.of(i,j));
  }

  private static void peekFinderOneDim() {
    // Peek Finder One dimension
    // b >= a and b >= c
    // P 9 is a peek if i >= h
    // P 0 is a peek if a >= b
    findPeek(new int[]{1, 2, 3, 2, 1}).ifPresent(System.out::println);
    findPeek(new int[]{2, 1, 1, 1, 1}).ifPresent(System.out::println);
    findPeek(new int[]{1, 2, 3, 4, 5}).ifPresent(System.out::println);
    findPeek(new int[]{2, 3, 4, 5, 4}).ifPresent(System.out::println);

    System.out.println("---");

    findPeekR(new int[]{1, 2, 3, 2, 1}).ifPresent(System.out::println);
    findPeekR(new int[]{2, 1, 0, 1, 1}).ifPresent(System.out::println);
    findPeekR(new int[]{1, 2, 3, 4, 5}).ifPresent(System.out::println);
    findPeekR(new int[]{2, 3, 4, 5, 4}).ifPresent(System.out::println);

    System.out.println("---");

    // Try on 10 millions
    int[] inputs = IntStreamEx.range(100_000_000).toArray();
    timeIt(i -> findPeek(i).ifPresent(System.out::print), inputs);
    timeIt(i -> findPeekR(i).ifPresent(System.out::print), inputs);
    timeIt(i -> findPeekTailR(i).ifPresent(System.out::print), inputs);
  }

  private static <T> void timeIt(Consumer<T> c, T t) {
    long startTime = System.nanoTime();
    c.accept(t);
    System.out.printf("%,12.2f ms%n", (System.nanoTime() - startTime) * NS_TO_MS);
  }

  // Straight forward version: Linear O(n) (best = 1 & worst = n)
  public static Optional<Integer> findPeek(int[] ip) {
    for (int i = 0; i < ip.length; i++)
      if ((i == 0 || ip[i] >= ip[i - 1]) && (i == ip.length - 1 || ip[i] >= ip[i + 1]))
        return Optional.of(i);
    return Optional.empty();
  }

  // Could solve with splitting in two: T(n/2) + O(1)
  // base case : T(1) = O(1)
  // T(n) = O(1) + ... + O(1) (log2n times) = O(Log2n)
  public static Optional<Integer> findPeekR(int[] ip) {
    return findPeekR(ip, 0, ip.length - 1);
  }

  private static Optional<Integer> findPeekR(int[] ip, int m, int n) {
    int i = (m + n) / 2;
    if (i > 0 && ip[i] < ip[i - 1]) return findPeekR(ip, m, i - 1);
    if (i < ip.length - 1 && ip[i] < ip[i + 1]) return findPeekR(ip, i + 1, n);
    return Optional.of(i);
  }

  public static Optional<Integer> findPeekTailR(int[] ip) {
    return findPeekTailR(ip, 0, ip.length - 1).invoke();
  }

  private static TailCall<Optional<Integer>> findPeekTailR(int[] ip, int m, int n) {
    int i = (m + n) / 2;
    if (i > 0 && ip[i] < ip[i - 1]) return () -> findPeekTailR(ip, m, i - 1);
    if (i < ip.length - 1 && ip[i] < ip[i + 1]) return () -> findPeekTailR(ip, i + 1, n);
    return done(Optional.of(i));
  }
}
