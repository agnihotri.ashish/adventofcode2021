package com.yoki.advent_of_code.sandbox.optaplanner.schooltimetabling.domain;


import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

@Getter
@NoArgsConstructor
@PlanningSolution
public class TimeTable {

  @ValueRangeProvider(id = "timeslotRange")
  @ProblemFactCollectionProperty
  private List<Timeslot> timeslots;

  @ValueRangeProvider(id = "roomRange")
  @ProblemFactCollectionProperty
  private List<Room> rooms;

  @PlanningEntityCollectionProperty
  private List<Lesson> lessons;

  @PlanningScore
  private HardSoftScore score;

  public TimeTable(List<Timeslot> timeslotList, List<Room> roomList, List<Lesson> lessonList) {
    this.timeslots = timeslotList;
    this.rooms = roomList;
    this.lessons = lessonList;
  }
}
