package com.yoki.advent_of_code.sandbox.optaplanner.schooltimetabling.domain;

import java.time.DayOfWeek;
import java.time.LocalTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Timeslot {
  private DayOfWeek dayOfWeek;
  private LocalTime startTime;
  private LocalTime endTime;

  @Override
  public String toString() {
    return dayOfWeek + " " + startTime;
  }
}
