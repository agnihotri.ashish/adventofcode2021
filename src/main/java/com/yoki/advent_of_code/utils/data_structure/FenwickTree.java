package com.yoki.advent_of_code.utils.data_structure;

import static java.lang.Integer.lowestOneBit;

import java.util.Arrays;

public class FenwickTree {

  private long[] tree;

  public FenwickTree(int sz) {
    tree = new long[sz + 1];
  }

  /**
   * Make sure the 'values' array is one base meaning values[0] does not get used, O(n) construction
   **/
  public FenwickTree(long[] values) {
    if (values == null)
      throw new IllegalArgumentException("Values array cannot be null!");

    this.tree = values.clone();
    for (int i = 1; i < tree.length; i++) {
      int j = i + lowestOneBit(i);
      if (j < tree.length) tree[j] += tree[i];
    }
  }

  /**
   * Compute the prefix sum for [1, i], one based
   */
  public long prefixSum(int i) {
    long sum = 0L;
    while (i != 0) {
      sum += tree[i];
      i &= ~lowestOneBit(i);
    }
    return sum;
  }

  /**
   * Returns the sum of the interval [i, j], one based
   **/
  public long sum(int i, int j) {
    if (j < i) throw new IllegalArgumentException("Make sure j >= i");
    return prefixSum(j) - prefixSum(i);
  }

  /**
   * Add 'k' to index 'i', one based
   **/
  public void add(int i, long k) {
    while (i < tree.length) {
      tree[i] += k;
      i += lowestOneBit(i);
    }
  }

  /**
   * Set index i to be equal to k, one based
   **/
  public void set(int i, long k) {
    add(i, k - sum(i, i));
  }

  @Override
  public String toString() {
    return Arrays.toString(tree);
  }
}
