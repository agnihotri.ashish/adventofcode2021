package com.yoki.advent_of_code.utils.vector;

import java.util.List;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.tuple.Pair;

public class Vector extends Tuple<Double> {

  public Vector(Double... toList) {
    super(toList);
  }

  public Vector(List<Double> toList) {
    super(toList);
  }

  public Vector sub(Vector other) {
    return new Vector(StreamEx.zip(this.zip(), other.zip(), Pair::of).map(p -> p.getLeft() - p.getRight()).toList());
  }

  public Vector add(Vector other) {
    return new Vector(StreamEx.zip(this.zip(), other.zip(), Pair::of).map(p -> p.getLeft() + p.getRight()).toList());
  }

  public Vector mul(double n) {
    return new Vector(StreamEx.of(this.zip()).map(v -> v.intValue() * n).toList());
  }

}
