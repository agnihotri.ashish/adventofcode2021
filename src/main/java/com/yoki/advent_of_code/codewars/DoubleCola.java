package com.yoki.advent_of_code.codewars;

public class DoubleCola {

  public static void main(String[] args) {
    // take the first elem and double it at the end of the list
    // "S", "L", "P", "R", "H"
    // "L", "P", "R", "H", "S", "S"
    // "P", "R", "H", "S", "S", "L", "L"
    // "R", "H", "S", "S", "L", "L", "P", "P"

    // "1S", "1L", "1P", "1R", "1H"
    // "1L", "P1", "1R", "1H", "2S"
    // "1P", "1R", "1H", "2S", "2L"
    // "1R", "1H", "2S", "2L", "2P"
    // "1H", "2S", "2L", "2P", "2R"
    // "2S", "2L", "2P", "2R", "2H"

    // 1: "S", "L", "P", "R", "H"
    // 2: "S", "L", "P", "R", "H"
    // 4: "S", "L", "P", "R", "H"
    // 8: "S", "L", "P", "R", "H"

    String[] names = {"Sheldon", "Leonard", "Penny", "Rajesh", "Howard"};

    System.out.println(whoIsNext(1, names));
    System.out.println(whoIsNext(5, names));
    System.out.println(whoIsNext(1040, names));
    System.out.println(whoIsNext(7740, names));
  }

  public static String whoIsNext(long n, String[] ns) {
    for (--n; n >= ns.length; n -= ns.length, n /= 2) ;
    return ns[(int) n];
  }
}
